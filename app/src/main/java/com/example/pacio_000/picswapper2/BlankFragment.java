package com.example.pacio_000.picswapper2;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pacio_000 on 25.06.2016.
 */
public class BlankFragment extends Fragment {
    private List<Linki> linki;
    public static SwipeRefreshLayout sfl;
    static RecyclerView rv = null;
    public BlankFragment(){
        getEtykiety();
    }
    private void getEtykiety(){
        linki=PlikiLista.mDataSet;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView=inflater.inflate(R.layout.fragment_blank,container,false);
         rv=(RecyclerView) rootView.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);
        MyAdapter adapter=new MyAdapter(linki);
        rv.setAdapter(adapter);
        rootView.setTag(Etykiety.index);
        Etykiety.index++;
        sfl=(SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        sfl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               // proces();
                if(PlikiLista.alternatywny==true){
                        Log.d("Refresh","zaszedl refresz i ustawiony alternatywny");
                        MyAdapter adapter1 = new MyAdapter(PlikiLista.mDataSet);
                        rv.setAdapter(adapter1);
                        rv.invalidate();
                        sfl.setRefreshing(false);
                        sfl.setRefreshing(false);
                }else {
                    PlikiLista.listaByl = new ArrayList<BylJuzTaki>();
                    List<Linki> list = PlikiLista.mDataSet;
                    Log.d("list11", "Wypisuje liste przed proces: ");
                    for (int n = 0; n < list.size(); n++) {
                        Log.d("list11", ":" + n + ": " + list.get(n).getAdres());
                    }
                    PlikiLista.kolejny.proces();

                    List<Linki> list2 = PlikiLista.mDataSet;
                    Log.d("list11", "Wypisuje liste po proces: ");
                    for (int n = 0; n < list2.size(); n++) {
                        Log.d("list11", ":" + n + ": " + list2.get(n).getAdres());
                    }
                    // PlikiLista.setMDataSeto();
                    List<Linki> list3 = PlikiLista.mDataSet;
                    Log.d("list11", "Wypisuje liste po proces: ");
                    for (int n = 0; n < list3.size(); n++) {
                        Log.d("list11", ":" + n + ": " + list3.get(n).getAdres());
                    }
                    MyAdapter adapter1 = new MyAdapter(PlikiLista.mDataSet);
                    rv.setAdapter(adapter1);
                    rv.invalidate();
                    sfl.setRefreshing(false);
                }
            }
        });

        sfl.setColorSchemeColors(Color.parseColor("#E50682"));
        LinearLayoutManager llm=new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        return rootView;

    }

}
