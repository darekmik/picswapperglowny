package com.example.pacio_000.picswapper2;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Patryk on 19.09.2016.
 */
public class Klikacz implements View.OnClickListener {
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    public ImageView mImageView;
    private Context cont;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    public Klikacz(ImageView mImageView,Context cont){
        this.mImageView=mImageView;
        this.cont=cont;
    }

    @Override
    public void onClick(View v) {
        int permission = ActivityCompat.checkSelfPermission(cont, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.d("premiszion","Nie masz premiszjion BREO :(");
            ActivityCompat.requestPermissions(
                    (Activity) cont,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        System.out.println("KLIK SHARE");
        Drawable drawable=mImageView.getDrawable();
        Bitmap bmp=null;
        if(drawable instanceof BitmapDrawable){
            bmp=((BitmapDrawable)mImageView.getDrawable()).getBitmap();
        }
        Uri bmpUri=null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG,0,fos);
            fos.close();
            bmpUri=Uri.fromFile(file);
        }catch(IOException e){
            e.printStackTrace();
        }

        Intent share=new Intent(Intent.ACTION_SEND);
        share.putExtra(Intent.EXTRA_STREAM,bmpUri);
        share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        share.setType("image/*");
        cont.startActivity(Intent.createChooser(share,"Share"));
    }

}
