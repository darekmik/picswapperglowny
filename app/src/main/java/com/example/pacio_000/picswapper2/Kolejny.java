package com.example.pacio_000.picswapper2;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Kolejny extends AppCompatActivity {
    private static Uzytkownik uz;
    private ListView list;
    //private ArrayAdapter<String> lista;
    private Context cont;
    private static Polaczenie4 pol;
    private static List<Linki> lista=null;
    private boolean mozna=false;
    private boolean dzialanie=false;
    private BlankFragment fragment;
    private CollapsingToolbarLayout col;
    private static boolean mozena=false;
    public int wysokosc;
    public CardView card;
    public static Context c;
    public SwipeRefreshLayout sfl;
    public static List<Linki> lista22;
    static OdbieraniePlikowLokalnie lol;
    public static boolean proecesEnd=false;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        cont=getApplicationContext();
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_kolejny);
        c=this;


        setContentView(R.layout.activity_kolejny);
      //  ActionBar bar = getActionBar();
       // bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#d3d3d3")));
       // Toolbar toolbar= (Toolbar) findViewById(R.id.toolbar);
      //  toolbar.setTitle("");
        TextView text=(TextView) findViewById(R.id.twojWidok);
        Typeface face= Typeface.createFromAsset(getAssets(),"fonts/Advent_Pro/AdventPro-Medium.ttf");
        Typeface face2= Typeface.createFromAsset(getAssets(),"fonts/Advent_Pro/AdventPro-Regular.ttf");
        TabLayout tabLayout= (TabLayout) findViewById(R.id.tab_layout);
       text.setTypeface(face);
        ViewGroup vg= (ViewGroup) tabLayout.getChildAt(0);
        for(int n=0;n<vg.getChildCount();n++){
            ViewGroup vg1=(ViewGroup) vg.getChildAt(n);
            for(int m=0;m<vg1.getChildCount();m++){
                View vg2=vg1.getChildAt(m);
                if(vg2 instanceof TextView){
                    Log.d("fontotesto","Poszlo zmianka");
                    ((TextView) vg2).setTypeface(face2);
                }
            }
        }

      //  TabLayout text1=(TabLayout) findViewById(R.id.tab_layout);
       // text1.
        //text.setTextColor(Color.BLACK);
        //TextView lolo= (TextView) findViewById(R.id.tv_text);
        //Typeface face= Typeface.createFromAsset(getAssets(),"fonts/Jura/Jura-Medium.ttf");
       // lolo.setTypeface(face);
        PlikiLista.kolejny=this;
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(Color.GRAY);
        setSupportActionBar(toolbar);
        ViewPager viewPager=(ViewPager) findViewById(R.id.viewpager);
        PagerAdapter pagerAdapter =new PagerAdapter(getSupportFragmentManager(),Kolejny.this);
        Etykiety.index=0;
        File pliko=new File(cont.getFilesDir().getAbsoluteFile().getPath());
      // lol=new OdbieraniePlikowLokalnie(pliko,uz);

//        lol.execute();
        viewPager.setAdapter(pagerAdapter);
      //  TabLayout tabLayout=(TabLayout)findViewById(R.id.tab_layout);
        tabLayout.setBackgroundColor(Color.GRAY);
        tabLayout.setupWithViewPager(viewPager);
        Etykiety.cont=getApplicationContext();

        getCont();
        for(int n=0;n<tabLayout.getTabCount();n++){
            TabLayout.Tab tab=tabLayout.getTabAt(n);

            tab.setCustomView(pagerAdapter.getTabView(n));
        }


        /*PlikiLista.pliczki=pliko.listFiles();
        File[] test=PlikiLista.pliczki;
        List<Linki> lista=new ArrayList<Linki>();
        Log.d("plikiZdysku", "Przed sprawdzaniem czy zawiera png");
        for(int n=0;n<test.length;n++){
            //boolean spro=false;
           Log.d("plikiZdysku", ": "+n+" : "+test[n].getName());
            String tmp=test[n].getName();
            char[] tabka=new char[tmp.length()];
            Log.d("charat",": wielkosc testu: "+test[n].getName().length());
            for(int m=0;m<test[n].getName().length();m++){
                tabka[m]=tmp.charAt(m);
            }

            if (test[n].getName().contains(".png")) {
                Log.d("plikiZdysku", "zawiera");
                Log.d("plikiZdysku", "plik: " + test[n].getAbsolutePath());
                lista.add(new Linki(test[n].getAbsolutePath(),uz,true,false));
              //  test[n].delete();
               // Log.d("testPliki","lista: "+lista.get(n).getAdresAndroid());
            }else  if (test[n].isDirectory() && !test[n].getName().equals("instant-run")){
                Log.d("plikiZdysku",": folder: "+test[n].getAbsolutePath());
               //usuwanie(test[n]);
                lista.add(new Linki(test[n].getAbsolutePath(),uz,true,true));
            }
        }
        Log.d("plikiZdysku", "Sprawdzanie listy przed wyslaniem do globalne");
        for(int n=0;n<lista.size();n++){
            Log.d("plikiZdysku", "lista: spr: "+n+lista.get(n).getAdresAndroid());
        }
        //Linki.lista=lista;
        if(PlikiLista.mDataSet==null){
            PlikiLista.mDataSet=new ArrayList<Linki>();
        }
        PlikiLista.mDataSet=lista;
       // PlikiLista.setMDataSeto();

         lista22=PlikiLista.mDataSet;

        Log.d("plikiZdysku","Rozmiar lista2: "+lista22.size());
        for(int n=0;n<lista22.size();n++){
            Log.d("plikiZdysku","lista: "+lista22.get(n).getAdresAndroid());
        }
        */
       // col=(CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
       // col.setTitle("Papaparapapa");
        //ImageView view =(ImageView) findViewById(R.id.header);
        //Bitmap bitmap= BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher);
        //Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
      /*   card=(CardView) findViewById(R.id.card_view);
        final Activity activity=(Activity) this;
        card.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener(){
            @Override
            public boolean onPreDraw(){
                card.getViewTreeObserver().removeOnPreDrawListener(this);
                wysokosc=card.getHeight();
                ViewGroup.LayoutParams layoutParams=card.getLayoutParams();
                layoutParams.height=(int) activity.getResources().getDimension(R.dimen.)
            }
        });
*/
       // list=(ListView) findViewById(R.id.listView);
   /*     Intent i =getIntent();
        final Uzytkownik uz= (Uzytkownik) i.getSerializableExtra("papa");
        PlikiLista.sciezka="/C:/PicSwaper/Users/U"+uz.getID();
        Log.d("testowaScieza","sciezka: "+PlikiLista.sciezka);
        int id=uz.getID();
        Log.d("dupa","W kolejny: "+Integer.toString(id));
        pol =new Polaczenie4(uz);
        pol.execute();
        Log.d("dupa","przed petla");
        try {
            boolean dzialajo=lol.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
      proces();
*/
     //   runPRoces();
       // View lol=findViewById(R.id.card_view);
        Intent i =getIntent();
        final Uzytkownik uz= (Uzytkownik) i.getSerializableExtra("papa");
        PlikiLista.uz=uz;
      /*  lol.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Log.d("dupa","Klik");
            }

        });
        */
        /*
        /*
        list.setNestedScrollingEnabled(false);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String adres = Kolejny.getListaNaPozycji(position).getAdres();

                Polaczenie3 pol = new Polaczenie3(adres, uz, getApplicationContext());
                pol.execute();
                Log.d("dupa", "przed dodaje do panelu");
                while(dzialanie==false) {
                    dzialanie=pol.getDzialanie();
                    if(dzialanie==true) {
                        Log.d("dzialanie", "Poszedl if");
                        Dater[] tabka=dodajeDoPanelu();
                        list.setAdapter(new Adapter(cont, tabka));
                    }
                }
            }
        });
        try {
            while (mozna == false) {
                mozna = pol.getMozna();
                if (mozna) {
                    lista = pol.getLinki();
                    Log.d("dupa", "Lista nie jest nullem pracuje");
                    ArrayList<Dater> nazwy = new ArrayList<Dater>();

                    for (Linki l : lista) {
                        File plik = new File(l.getAdres());
                        String sciezka=plik.getName().substring(plik.getName().lastIndexOf("\\")+1);

                        Log.d("dupa","sciezka: "+File.separator);
                        nazwy.add(new Dater(sciezka,false));
                        Log.d("dupa", "nazwa: " + sciezka.substring(sciezka.lastIndexOf(File.separator)+1));
                    }
                    File plik=new File(getApplicationContext().getFilesDir().getAbsolutePath());
                    List<Dater> pliki=new ArrayList<Dater>();
                    for(String s : plik.list()){
                        pliki.add(new Dater(s,true));
                    }
                    for(Dater s: pliki){
                        nazwy.add(s);
                    }
                    //  ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.wiersz,nazwy);
                    Dater[] tablica = new Dater[nazwy.size()];
                    int p = 0;
                    for (Dater s : nazwy) {
                       // String tmp = s;
                        tablica[p]=s;
                        p++;
                    }
                    list.setAdapter(new Adapter(this, tablica));

                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
*/

    }
    void usuwanie(File plik){
        if(plik.isDirectory()){
            for(File child : plik.listFiles()){
                usuwanie(child);
            }
        }
        plik.delete();
    }

    public void getCont(){
        Etykiety.cont=this;

    }
    public static void runPRoces(Uzytkownik uz,OdbieraniePlikowLokalnie lol){
      //  Intent i =getIntent();
       // final Uzytkownik uz= (Uzytkownik) i.getSerializableExtra("papa");
        PlikiLista.sciezka="/C:/PicSwaper/Users/U"+uz.getID();
        Log.d("testowaScieza","sciezka: "+PlikiLista.sciezka);
        int id=uz.getID();
        Log.d("dupa","W kolejny: "+Integer.toString(id));
        pol =new Polaczenie4(uz);
        pol.execute();
        Log.d("dupa","przed petla");
        try {
            if(!lol.isCancelled()) {
                boolean dzialajo = lol.get();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        proces();
        try {
            pol.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
    public static List<Linki> transformacja(List<TypyFile> lista){
        List<Linki> linki=new ArrayList<Linki>();
        for(int n=0;n<lista.size();n++){
            TypyFile typ=lista.get(n);
            if(typ.getTyp()== FolderWorker.Typ.Plik){
                linki.add(new Linki(typ.getFile().getAbsolutePath(),uz,false));
            }else{
                linki.add(new Linki(typ.getFile().getAbsolutePath(),uz,true));
            }
        }
        return linki;
    }
    public static void proces(){
        Log.d("zaszlo","Jestem w proces");
        do{
            Log.d("dupa","krece w zapytaniu o plik");
            if(pol.getDzialanie()==true) {
                Log.d("zaszlo12342","Polaczenie 4 poszlo");
                System.out.println("Polaczono2");

                List<Linki> linkio = transformacja(pol.getLista());
                if(PlikiLista.mDataSet==null){
                    PlikiLista.mDataSet=new ArrayList<Linki>();
                }
                /*
                Log.d("proces","Przed fragmentem");
                    File plik1=null;
                    File plik2=null;
                    for(int n=0;n<linkio.size();n++){
                        boolean zaszlo=false;
                        for(int m=0;m<PlikiLista.mDataSet.size();m++){
                            plik1=null;
                            plik2=null;
                            if(linkio.get(n).getAdres()!=null ) {
                               // Log.d("proces", "W procesie zaszedl pierwszy if dwa adresy nie sa nullami");
                                plik1 = new File(linkio.get(n).getAdres());
                            }
                            if( PlikiLista.mDataSet.get(m).getAdres() !=null) {
                                plik2 = new File(PlikiLista.mDataSet.get(m).getAdres());
                            }
                            if(plik1!=null && plik2 !=null) {
                                Log.d("proces", "spr: plik1: " + plik1.getName() + " != " + plik2.getName());
                                if (!plik1.getName().equals(plik2.getName())) {
                                    Log.d("proces", "zaszlo");
                                    PlikiLista.mDataSet.add(linkio.get(n));
                                    zaszlo=true;
                                }
                            }
                            Log.d("proces","m: "+m+" \t zaszlo: "+zaszlo);
                            Log.d("proces","rozmair mdataset: "+PlikiLista.mDataSet.size());
                            if(m==PlikiLista.mDataSet.size()-1 && zaszlo == false){
                                Log.d("proces","Zaszedl if awarykny");
                                PlikiLista.mDataSet.add(linkio.get(n));
                            }
                        }
                    }
                        */



               for(int m=0;m<linkio.size();m++){
                   File pliko=new File(linkio.get(m).getAdres());
                   Log.d("TestoLinko",": adres przed: "+pliko.getAbsolutePath());
                   String adres=pliko.getParentFile().getAbsolutePath();
                   Log.d("TestoLinko",": adres: "+adres+" ==  PlikLista.sciezka: "+PlikiLista.sciezka);
                   if(adres.equals(PlikiLista.sciezka)) {

                       PlikiLista.mDataSet.add(linkio.get(m));
                   }

                }
  /*
                for(int m=0;m<PlikiLista.mDataSet.size();m++){
                    if(PlikiLista.mDataSet.get(m).getAdresAndroid()!=null) {
                        Log.d("sprawdzanie212", ": " + m + ":adres android :" + PlikiLista.mDataSet.get(m).getAdresAndroid());
                    }
                    if(PlikiLista.mDataSet.get(m).getAdres()!=null) {
                        Log.d("sprawdzani212", ": " + m + ":adres :" + PlikiLista.mDataSet.get(m).getAdres());
                    }
                 //   Log.d("sprawdzanie212", ": " + m + ":Uzywane :" + PlikiLista.mDataSet.get(m).getUzywane());
                }

             final List<Linki> listka=PlikiLista.mDataSet;
                File plik=null;
                File plik2=null;
                for(int m=0;m<linkio.size();m++) {
                    for (int n = 0; n < listka.size(); n++) {
                        plik=null;
                        plik2=null;
                        //   Log.d("zaszlo",linkio.get(n).getAdresAndroid()+" == "+listka.get(n).getAdresAndroid());
                        plik2 = new File(linkio.get(m).getAdres());
                        if(listka.get(n).getAdresAndroid()!=null) {
                             plik = new File(listka.get(n).getAdresAndroid());
                             plik2 = new File(linkio.get(m).getAdres());
                            Log.d("sprPlikow", plik.getName() + " == " + plik2.getName());
                        }
                        else if(plik !=null && plik2.getName().equals(plik.getName())) {

                            //linkio.get(m).setUzywane(true);
                            Log.d("sprPlikow", "Zaszlo dla: " + PlikiLista.mDataSet.get(m).getAdres());
                        } else if(plik2 !=null && plik==null){
                            Log.d("sprPlikow", "Nie zaszlo");
                           // linkio.get(m).setUzywane(false);
                            PlikiLista.mDataSet.add(linkio.get(m));
                        }
                    }
                }
                */
               // PlikiLista.mDataSet=PlikiLista.lista;
                for(int m=0;m<linkio.size();m++){
                    Log.d("TestoLinko2",": Linkio : "+m+" : "+linkio.get(m).getAdres());
                   // PlikiLista.lista2.add(linkio.get(m));
                }
                PlikiLista.setMDataSeto();
                if(PlikiLista.pierwszy==false) {
                    //PlikiLista.lista2 = new ArrayList<Linki>();
                    for(int m=0;m<linkio.size();m++){
                        Log.d("TestoLinko2",": Linkio : "+m+" : "+linkio.get(m).getAdres());
                        PlikiLista.lista2.add(linkio.get(m));
                    }
                    for(int n=0;n<PlikiLista.mDataSet.size();n++){

                        PlikiLista.lista2.add(PlikiLista.mDataSet.get(n));
                    }
                    for(int n=0;n<lista22.size();n++){
                        PlikiLista.lista2.add(lista22.get(n));
                    }

                    Log.d("testoweWypisanieListy11","Wypisuje liste testowow przed smieciarzem: ");
                   for(int m=0;m<PlikiLista.lista2.size();m++){
                        if(PlikiLista.lista2.get(m).getAdresAndroid()!=null) {
                            Log.d("testoweWypisanieListy11", ": adres andorid" + m + " : " + PlikiLista.lista2.get(m).getAdresAndroid());
                        }
                        if(PlikiLista.lista2.get(m).getAdres()!=null) {
                            Log.d("testoweWypisanieListy11", ": adres" + m + " : " + PlikiLista.lista2.get(m).getAdres());
                        }
                    }
                    PlikiLista.listaByl=new ArrayList<BylJuzTaki>();
                   PlikiLista.lista2=CollectionSmieciarz.work(PlikiLista.lista2);

                    Log.d("testoweWypisanieListy22","Wypisuje liste testowow po smieciarzu: ");
                    for(int m=0;m<PlikiLista.lista2.size();m++){
                        if(PlikiLista.lista2.get(m).getAdresAndroid()!=null) {
                            Log.d("testoweWypisanieListy22", ": adres andorid" + m + " : " + PlikiLista.lista2.get(m).getAdresAndroid());
                        }
                        if(PlikiLista.lista2.get(m).getAdres()!=null) {
                            Log.d("testoweWypisanieListy22", ": adres" + m + " : " + PlikiLista.lista2.get(m).getAdres());
                        }
                    }
                    PlikiLista.pierwszy=true;
                }
                Log.d("sprawdzanie", "===========================Wypisywanie po set mdataset========================");
                for(int m=0;m<PlikiLista.mDataSet.size();m++){
                    if(PlikiLista.mDataSet.get(m).getAdresAndroid()!=null) {
                        Log.d("sprawdzanie", ": " + m + ":adres android :" + PlikiLista.mDataSet.get(m).getAdresAndroid());
                    }
                    if(PlikiLista.mDataSet.get(m).getAdres()!=null) {
                        Log.d("sprawdzanie", ": " + m + ":adres :" + PlikiLista.mDataSet.get(m).getAdres());
                    }
                    Log.d("sprawdzanie", ": " + m + ":Uzywane :" + PlikiLista.mDataSet.get(m).getUzywane());
                }
                Log.d("rozm","Rozmiar mdDataSet w kolejny: "+PlikiLista.mDataSet.size());
                //Linki.lista=linkio;
                //PlikiLista.setMDataSet();
                //PlikiLista.mDataSet=linkio;

            //   getCont();
                System.out.println("Rozmiar: "+linkio.size());
              /*  String[] etykiety = new String[linkio.size()+1];
                for (int s = 0; s < linkio.size(); s++) {
                    File plik=new File(linkio.get(s).getAdres());
                    etykiety[s] = plik.getName();

                    System.out.println("dodano do tablicy: "+ plik.getName());
                }

                Etykiety.etykeiety=etykiety;*/

                Log.d("plikiS", "Wypisuje:");
                for (int c = 0; c < linkio.size(); c++) {
                    Log.d("plikoS", linkio.get(c).getAdres());
                  //  Log.d("plikiS",linkio.get(c).getUzywane());
                }
                mozena=true;
                Log.d("dupa","Mozena true");
                proecesEnd=true;
            }
        }while(pol.getDzialanie()==false);
       // MyAdapter.
    }
    public static void setIntent(Class cl){
        Intent in=new Intent(c,cl.getClass() );
        c.startActivity(in);
    }
    @Override
    public void onBackPressed(){
        if(PlikiLista.alternatywny==false){
            finish();
        }
        PlikiLista.undoView();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_kolejny, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.d("dupa","Klik"+id);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public  Uzytkownik getUzytkownik(){return uz;}
    public List<Linki> getLista(){
       return lista;
    }
    public static Linki getListaNaPozycji(int pozycja){
        return lista.get(pozycja);
    }

    public  Dater[] dodajeDoPanelu(){
        Log.d("dupa","W dodaje do apnelu");
        File plik=new File(getApplicationContext().getFilesDir().getAbsolutePath());
        List<String> pliki=new ArrayList<String>();
        for(String s : plik.list()){
            pliki.add(s);
        }
        Dater[] tablica=new Dater[pliki.size()];
        int n=0;
        for(String s : pliki){
            System.out.println("Wypisuje: "+s);
            String data=s;
            tablica[n]=new Dater(data,true);
            Log.d("dupa","Wypisuje: "+s);
            n++;
        }
        return tablica;
    }
    class PagerAdapter extends FragmentPagerAdapter{
        String[] tytuly=new String[] {"Pierwsza","Druga","Trzecia"};
        Context context;

        public PagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context=context;
        }

        @Override
        public Fragment getItem(int position) {
            while (mozena == false) {
              //  System.out.println("Krece");
                if(mozena==true){
                    Log.d("dupa", "Zaszlo true");
                    return new BlankFragment();

                }
            }
            return new BlankFragment();
        }

        @Override
        public int getCount() {
            return tytuly.length;
        }
        @Override
        public CharSequence getPageTitle(int position){
            return tytuly[position];
        }
        public View getTabView(int position){
            View tab= LayoutInflater.from(Kolejny.this).inflate(R.layout.custom_tab,null);

            TextView tv=(TextView) tab.findViewById(R.id.custom_text);
            Typeface face= Typeface.createFromAsset(getAssets(),"fonts/Lato/Lato-Medium.ttf");
         //   Typeface face2= Typeface.createFromAsset(getAssets(),"fonts/Advent_Pro/AdventPro-Regular.ttf");
            tv.setTypeface(face);
           // tv.setGravity(Gravity.CENTER_VERTICAL);
            tv.setText(tytuly[position]);
            return tab;
        }
    }
}
