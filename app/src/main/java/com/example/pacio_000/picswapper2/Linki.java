package com.example.pacio_000.picswapper2;

import java.util.List;

public class Linki {
    public static List<Linki> lista;
    private boolean folder;
    private String adres;
    private boolean uzywane=false;
    private String adresAndroid;
    private Uzytkownik uz;
    public Linki(String adres,Uzytkownik uz,boolean folder){
        this.adres=adres;
        this.uz=uz;
        this.folder=folder;
    }
    public Linki(String adresAndroid, Uzytkownik uz, boolean uzywane,boolean folder){
        this.adresAndroid=adresAndroid;
        this.uz=uz;
        this.uzywane=uzywane;
        this.folder=folder;
    }
    public Linki(String adresAndroid, String adres, Uzytkownik uz,boolean uzywane,boolean folder){
        this.adresAndroid=adresAndroid;
        this.adres=adres;
        this.uz=uz;
        this.uzywane=uzywane;
        this.folder=folder;
    }
    public Linki(){ }
    public String getAdres() {
        return adres;
    }
    public void setAdres(String adres) {
        this.adres = adres;
    }
    public Uzytkownik getUzytkownik(){
        return uz;
    }
    public boolean getUzywane(){
        return uzywane;
    }
    public void setUzywane(boolean uzywane){
        this.uzywane=uzywane;
    }
    public String getAdresAndroid(){
        return adresAndroid;
    }
    public void setAdresAndroid(String adres){
        adresAndroid=adres;
    }
    public boolean getFolder(){
        return folder;
    }

}
