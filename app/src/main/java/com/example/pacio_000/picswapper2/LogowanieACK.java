package com.example.pacio_000.picswapper2;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.concurrent.ExecutionException;

public class LogowanieACK extends AppCompatActivity {
    EditText login;
    EditText haslo;
    TextView text,text2,text3;
    Polaczenie pol;
    boolean koniec=false;
    private Uzytkownik uz=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logowanie_ack);
        login=(EditText)findViewById(R.id.editText);
        haslo=(EditText)findViewById(R.id.editText);
        //vido=(VideoView) findViewById(R.id.videoView);
        ImageView im2=(ImageView) findViewById(R.id.imageView2);
        //   text=(TextView) findViewById(R.id.textView4);
        text2=(TextView) findViewById(R.id.textView);
        text3=(TextView) findViewById(R.id.textView2);
//        text.setText("PicSwapper");
        //   text.setTextSize(35);
        Typeface face2= Typeface.createFromAsset(getAssets(),"fonts/Lato/Lato-Medium.ttf");
        Typeface face= Typeface.createFromAsset(getAssets(),"fonts/Monoton/Monoton-Regular.ttf");
        text2.setTypeface(face2);
        login.setTypeface(face2);
        haslo.setTypeface(face2);
        text3.setTypeface(face2);

        //   text.setTypeface(face);
        im2.setImageResource(R.mipmap.ic_launcher);
        //    text.setTextColor(Color.BLACK);
        Button logowanie=(Button)findViewById(R.id.button);
        logowanie.setTypeface(face2);
        logowanie.setBackgroundColor(Color.BLACK);
        logowanie.setTextColor(Color.WHITE);
        logowanie.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                pol =new Polaczenie(login.getText().toString(),haslo.getText().toString());

                pol.execute();

                System.out.println("Polaczono");

                    // System.out.println("Krece w mineActivity");
                try {
                    uz = pol.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                if(uz!=null) {
                        System.out.println("Poszlo");
                        Intent i2=getIntent();
                        String uri= (String) i2.getSerializableExtra("uri");
                         Log.d("adred13233",": Odbieram w logowanieACK: "+uri);
                        Intent i = new Intent(getApplicationContext(), ObrazkoAkceptor.class);
                        i.putExtra("uri",uri);
                        i.putExtra("papa",uz);
                        Log.d("logowanieTesto","Imie uz: "+uz.getImie());
                        Log.d("logowanieTesto","ID uzytkownika: "+uz.getID());
                        PlikiLista.uz=uz;
                        koniec=true;
                        startActivity(i);
                        finish();
                    }


            }
        });
    }
}
