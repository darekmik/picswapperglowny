package com.example.pacio_000.picswapper2;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pacio_000 on 25.06.2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private static List<Linki> mDataSet;
    public  static CardView card;
    public static String adreso;
    public static Context cont;
    public List<Linki> listka;
    public Button przycisk;
    public Button przycisk2;
    public Button buttonS;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    // public static View itemView;
    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public CardView mCardView;
        public TextView mTextView;
        public ImageView mImageView;
       public TextView mTextView2;

        public static void verifyStoragePermissions(Activity activity) {
            // Check if we have write permission
            int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                Log.d("premiszion","Nie masz premiszjion BREO :(");
                ActivityCompat.requestPermissions(
                        activity,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );
            }
        }

       // public CardView card;
        public int wysokosc;

        public MyViewHolder(final View itemView) {
            super(itemView);
           // this.itemView=itemView;
            mCardView=(CardView) itemView.findViewById(R.id.card_view);
            mTextView=(TextView) itemView.findViewById(R.id.tv_text);
            final Button buttonS= (Button) itemView.findViewById(R.id.button6);

            mImageView=(ImageView) itemView.findViewById(R.id.iv_image);

            Typeface face2= Typeface.createFromAsset(Etykiety.cont.getAssets(),"fonts/Lato/Lato-Medium.ttf");
            Typeface face3= Typeface.createFromAsset(Etykiety.cont.getAssets(),"fonts/Lato/Lato-Light.ttf");
            mTextView.setTypeface(face2);
            card=(CardView) itemView.findViewById(R.id.card_view);
            mTextView2=(TextView) itemView.findViewById(R.id.tv_blah);
            mTextView2.setTypeface(face3);
            itemView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener(){
                @Override
               public boolean onPreDraw(){
                    Log.d("cos","Cos dziala");
                   itemView.getViewTreeObserver().removeOnPreDrawListener(this);
                    wysokosc=card.getHeight();
                    ViewGroup.LayoutParams layoutParams=itemView.getLayoutParams();
                    layoutParams.height=170;
                    itemView.setLayoutParams(layoutParams);
                    return true;
               }
            });


            final List<Linki> listka=PlikiLista.mDataSet;
            Log.d("adresy","Wypisuje adresy:");
            for(int n=0;n<listka.size();n++){
                Log.d("adresy","adres: "+listka.get(n).getAdresAndroid());
            }
            cont=Etykiety.cont;
            int pozycja=Etykiety.index;
//            int pozycja2= (int) itemView.getTag();

            CardView ilosc= (CardView) itemView.findViewById(R.id.card_view);
            //ilosc.g
            Log.d("id","id view: "+itemView.getId());
            Log.d("id","pozycja: "+pozycja);
            for(int m=0;m<PlikiLista.mDataSet.size();m++){
                if(PlikiLista.mDataSet.get(m).getAdres()!=null) {
                    Log.d("TestoMdataseto", ": adres :" + m + " : " + PlikiLista.mDataSet.get(m).getAdres());
                }
                if(PlikiLista.mDataSet.get(m).getAdresAndroid()!=null) {
                    Log.d("TestoMdataseto", ": adresAndroid :" + m + " : " + PlikiLista.mDataSet.get(m).getAdresAndroid());
                }
            }
      //      Log.d("id","pozycja2: "+pozycja2);
          //  int pozycja= (int) ilosc.getTag();
/*
            for(int n=0;n<listka.size();n++) {


                File pliko=new File(listka.get(n).getAdresAndroid());
                Log.d("testyWPetli","Pozycja: "+pozycja+" Nazwa pliko: "+pliko.getName());
                if (pliko.getName().contains(".png") && pozycja==n){
                    Log.d("testyWPetli","Zaszlo \t SAMBA");
                    //Log.d("pliczko", "plik: " + pliczko[n]);

                File obraz = new File(pliko.getPath());
                //  mDataSet.get(pozycja).setAdresAndroid(obraz.getAbsolutePath());
                // if(obraz.exists()){
                mTextView2.setText("");
                System.out.println("Pokazuje w adapterze");
                Log.d("widze", "Obrazo");
                Bitmap bitmap = BitmapFactory.decodeFile(obraz.getAbsolutePath());
                Log.d("obraz", "informacje: " + bitmap.getHeight() + " x " + bitmap.getWidth());
                if (bitmap.getHeight() > bitmap.getWidth()) {
                    // bitmap.setWidth(100);
                    //  bitmap.setHeight(150);
                    mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, false));
                } else {
                    //  bitmap.setWidth(150);
                    //  bitmap.setHeight(100);
                    mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, false));
                }
                //  mImageView.setImageBitmap(bitmap);
                //CardView card=(CardView) itemView.findViewById(R.id.card_view);
                    toggleProductDescriptionHeight(itemView);
                card.setMinimumHeight(100);
                card.setCardBackgroundColor(Color.BLUE);
                card.invalidate();
                itemView.invalidate();
            }else{
                    Log.d("testyWPetli","Nie poszlo zapytanie :(");
                }
            }
            */
            Log.d("zachodzi123@","Interfejs testowy zobacz gdzie trafisz:");
            Etykiety.index++;
            itemView.findViewById(R.id.iv_image);
            itemView.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                  //  int nowaPozycja= (int) card.getTag();

                   // int id=  itemView.getId();
                    CardView lol= (CardView) itemView.findViewById(R.id.card_view);
                   // int id2=lol.getId();
                  //  Log.d("pozycja", "id: "+id);
                  //  Log.d("pozycja", "id2: "+id2);
                   int pozycja= (int) lol.getTag();
                   Log.d("pozycja", "Pozycja: "+pozycja);
                   // int pozycja=v.getVerticalScrollbarPosition();
                    Log.d("dupaP", String.valueOf(v.getVerticalScrollbarPosition()));

                    Log.d("dupaP", mDataSet.get(pozycja).getAdres());
              //      Log.d("dupaP", mDataSet.get(pozycja).getUzytkownik().getImie());

                    Log.d("dupaP","Klik");
                    Log.d("spr1232",": getFolder: "+mDataSet.get(pozycja).getFolder() + " getUzywane: "+mDataSet.get(pozycja).getUzywane() );
                    if(mDataSet.get(pozycja).getUzywane()==false && mDataSet.get(pozycja).getFolder()==false ) {
                        Log.d("spr1232",": ZASZLO ZWYKLE: " );
                        boolean wykonany=false;
                        Log.d("zachodzi","Zachodzi polaczenie musi byc getuzywane==false");
                        Polaczenie3 polo = new Polaczenie3(mDataSet.get(pozycja).getAdres(), mDataSet.get(pozycja).getUzytkownik(), Etykiety.cont);
                        polo.execute();
                        Log.d("spr1232",": ZA POLACZENIE 3: " );
                        System.out.println("Przed while");
                        do {
                           System.out.println("Krece czekajac na obrazek");
                            Log.d("Polacznie3","Czekam na obrazek z polacznia 3");
                            if (polo.getDzialanie() == true) {
                                Log.d("Polacznie3","Zaszedl true");
                                Log.d("zachodzi123@","Trafilem 1 false false");
                               // polo.cancel(false);
                            //    PlikiLista.tmpView(PlikiLista.adres);
                                toggleProductDescriptionHeight(itemView);
                                final File obraz = new File(polo.getSciezka());
                                mDataSet.get(pozycja).setAdresAndroid(obraz.getAbsolutePath());
                                // if(obraz.exists()){
                                mTextView2.setText("");
                                System.out.println("Pokazuje w adapterze");
                                Log.d("widze", "Obrazo");
                                final Bitmap bitmap = BitmapFactory.decodeFile(obraz.getAbsolutePath());

                                Log.d("obraz", "informacje: " + bitmap.getHeight() + " x " + bitmap.getWidth());
                                if (bitmap.getHeight() > bitmap.getWidth()) {
                                    // bitmap.setWidth(100);
                                    //  bitmap.setHeight(150);
                                    mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, false));
                                } else {
                                    //  bitmap.setWidth(150);
                                    //  bitmap.setHeight(100);
                                    mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, false));
                                }
                                //  mImageView.setImageBitmap(bitmap);
                                //CardView card=(CardView) itemView.findViewById(R.id.card_view);
                               // Klikacz.mImageView=mImageView;
                                itemView.findViewById(R.id.button6).setOnClickListener(new Klikacz(mImageView,cont));
                               // buttonS.setOnClickListener(new Klikacz(mImageView,cont));
                                card.setMinimumHeight(100);
                                card.setCardBackgroundColor(Color.BLUE);
                                card.invalidate();
                                itemView.invalidate();
                                toggleProductDescriptionHeight(itemView);

                                //    }
                                wykonany=true;
                            }
                           // Log.d("zabawka222","polo.getDzialanie(): "+polo.getDzialanie()+"\n wykonany: "+wykonany);
                        } while (polo.getDzialanie() == false && wykonany==false);
                        mDataSet.get(pozycja).setUzywane(true);

                    }

                    else if( mDataSet.get(pozycja).getFolder()==true && mDataSet.get(pozycja).getUzywane() == false) {
                        Log.d("spr1232",": ZASZLO DLA NIE UZYWANEGO FOLDERU: " );
                        Log.d("spr1232",": ADRES:  "+ mDataSet.get(pozycja).getAdres());
                        //Log.d("spr1232",": :  "+ mDataSet.get(pozycja).getAdres());
                        Polaczenie3 polo = new Polaczenie3(mDataSet.get(pozycja).getAdres(), mDataSet.get(pozycja).getUzytkownik(), Etykiety.cont);
                        polo.execute();
                        boolean poszlo=false;
                        do {

                        //   System.out.println("Krece czekajac na obrazek");
                          //  Log.d("polodzia","Krece");

                            if (polo.getDzialanie() == true) {
                                Log.d("zachodzi123@","Trafilem 2 true false");
                                Log.d("polodzia","Dziala true");
                                Log.d("adreso123","Sciezka z polaczemoa: "+polo.getSciezka());
                                Log.d("adreso123","Sciezka w plik listaa: "+ PlikiLista.adres);
                             //   PlikiLista.adres= polo.getSciezka();
                                Log.d("spr12323",": ZASZLO w nieuzywanm ture: " );
                                toggleProductDescriptionHeight(itemView);

                                File obraz = new File(polo.getSciezka());
                           //     mDataSet.get(pozycja).setAdresAndroid(obraz.getAbsolutePath());
                                mDataSet.get(pozycja).setAdresAndroid(obraz.getAbsolutePath());
                                Log.d("folder1","JEST FOLDER");
                                Log.d("folder1",": mdAataSet adres: "+mDataSet.get(pozycja).getAdres());
                                Log.d("onbindviewholder", "Zaszlo ");
                                mTextView2.setText("");
                                //mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, false));
                                itemView.findViewById(R.id.button6).setOnClickListener(new Klikacz(mImageView,cont));
                             //   buttonS.setOnClickListener(new Klikacz(mImageView,cont));
                                Bitmap bitmap = BitmapFactory.decodeResource(cont.getResources(),R.drawable.ic_folder2);
                                mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, true));
                                card.setMinimumHeight(100);
                                card.setCardBackgroundColor(Color.BLUE);
                                card.invalidate();
                                itemView.invalidate();
                                poszlo=true;
                            }
                            //Log.d("sprooo","dzialanie: "+polo.getDzialanie()+" poszlo: "+poszlo);
                        } while (polo.getDzialanie() == false && poszlo==false);
                        Log.d("spr1232",": Za while" );
                        mDataSet.get(pozycja).setUzywane(true);

                    }else if(mDataSet.get(pozycja).getFolder()==true && mDataSet.get(pozycja).getUzywane() == true){
                        Log.d("spr1232","FOLDER JEST NA DYSKU");
                        Log.d("zachodzi123@","Trafilem 3 true true");
                        //PlikiLista.alternatywny=true;
                       // BlankFragment.sfl.setRefreshing(true);
                        toggleProductDescriptionHeight(itemView);

                        PlikiLista.adres=mDataSet.get(pozycja).getAdresAndroid();
                        Log.d("adreso123","adres jeszcze w myAdapter"+PlikiLista.adres);
                        PlikiLista.tmpView(PlikiLista.adres);
                        itemView.findViewById(R.id.button6).setOnClickListener(new Klikacz(mImageView,cont));
                       // buttonS.setOnClickListener(new Klikacz(mImageView,cont));
                        card.setMinimumHeight(100);
                        card.setCardBackgroundColor(Color.BLUE);
                        card.invalidate();
                        itemView.invalidate();

                    }
                    else{
                        Log.d("spr1232","Poszedl else");
                        //Intent intent=new Intent(Kolejny.,Obrazki.class);
                        Log.d("zachodzi123@","Trafilem ELSE true true");
                        Log.d("zachodzi123@","Zaszedl else getuUzywane musi byc true");
                        Log.d("zachodzi123@","mdataset.getuzywane: "+mDataSet.get(pozycja).getUzywane());
                        Log.d("zachodzi123@","mdataset.getadresadnroid: "+mDataSet.get(pozycja).getAdresAndroid());
                        Log.d("zachodzi123@","mdataset.getadres: "+mDataSet.get(pozycja).getAdres());
                        //buttonS= (Button) itemView.findViewById(R.id.button6);
                        itemView.findViewById(R.id.button6).setOnClickListener(new Klikacz(mImageView,cont));
                        //buttonS.setOnClickListener(new Klikacz(mImageView,cont));
                        adreso=mDataSet.get(pozycja).getAdresAndroid();
                        if(adreso==null){
                            for(int p=0;p<listka.size();p++){
                                File plik=new File(mDataSet.get(pozycja).getAdres());
                                File plik2=new File(listka.get(p).getAdresAndroid());
                                if(plik.getName().contains(plik2.getName())){
                                    adreso=plik2.getAbsolutePath();
                                }
                            }
                            Log.d("set","Ustawiono na :"+adreso);
                        }
                        Obrazki obraz=new Obrazki(adreso);
                      // Kolejny.setIntent(obraz.getClass());
                        card.setMinimumHeight(100);
                        card.setCardBackgroundColor(Color.BLUE);
                        card.invalidate();
                        itemView.invalidate();
                        Log.d("umc","DUPA DUPA DUPA");
                    }

                }

            });
        }
    }

    public  static void toggleProductDescriptionHeight(final View itemView){
        Log.d("cos","Cos funkcja tez rusza");
        int descriptionViewMinHeight=62;
        if(descriptionViewMinHeight == 62){
            Log.d("cos","zaszedl if");
            ValueAnimator anim= ValueAnimator.ofInt(itemView.getMeasuredHeightAndState(),250);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(){
               public void onAnimationUpdate(ValueAnimator valueAnimator){
                   int val=(Integer) valueAnimator.getAnimatedValue();
                   ViewGroup.LayoutParams layoutParams=itemView.getLayoutParams();
                   layoutParams.height=val;
                   itemView.setLayoutParams(layoutParams);
                   //itemView.setCardBackgroundColor(Color.BLUE);
               }
            });
            anim.start();
        }else{
            Log.d("cos","zaszedl else");
            ValueAnimator anim= ValueAnimator.ofInt(card.getMeasuredHeightAndState(),card.getHeight());
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(){
                public void onAnimationUpdate(ValueAnimator valueAnimator){
                    int val=(Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams=card.getLayoutParams();
                    layoutParams.height=val;
                    card.setLayoutParams(layoutParams);
                }
            });
            anim.start();
        }
    }
    public MyAdapter(List<Linki> mDataSet){
        Log.d("dupa","Adapter poszedl !");
        this.mDataSet=mDataSet;
    }
    public MyAdapter(){}

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item,parent,false);
        MyViewHolder mvh=new MyViewHolder(v);
        v.setTag(Etykiety.index);
        int pozycja=Etykiety.index;


        Etykiety.index++;
        return mvh;
    }
    public static List<Linki> getMDataSet(){
        return mDataSet;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Log.d("listaBind","Wypisuje liste bind:");
        for(int n=0;n<mDataSet.size();n++){
            if(mDataSet.get(n).getAdres()!=null) {
              //  Log.d("listaBind", ": adres android: "+mDataSet.get(n).getAdresAndroid());
            }
            if(mDataSet.get(n).getAdres()!=null) {
          //      Log.d("listaBind", ": adres: "+mDataSet.get(n).getAdres());
            }
        //    Log.d("listaBind", ": uzywane: "+mDataSet.get(n).getUzywane());
         //   Log.d("listaBind", ": folder: "+mDataSet.get(n).getFolder());
        }
     //   Log.d("testoBind",""+mDataSet.get(position).getAdres());
        holder.mCardView.setTag(position);
        File plik;
        if(mDataSet.get(position).getAdres()!=null) {
             plik = new File(mDataSet.get(position).getAdres());
        }else{
            plik=new File(mDataSet.get(position).getAdresAndroid());
        }
        holder.mTextView.setText(plik.getName());
       // listka=PlikiLista.lista;
       // listka=mDataSet;
       /* for(int m=0;m<mDataSet.size();m++) {
            Log.d("lista2", "Wypisuje liste:");
            if (mDataSet.get(m).getAdres() != null) {
                Log.d("lista2", "getAdres: " + mDataSet.get(m).getAdres());
                // mDataSet.get(position).setUzywane(false);
            }
            if (mDataSet.get(m).getAdresAndroid() != null) {
                Log.d("lista2", "getAdresAndroid: " + mDataSet.get(m).getAdresAndroid());
            }
            Log.d("lista2", "folder: " + mDataSet.get(m).getFolder());
        }*/
     //   Log.d("onbindviewholder","getUzywane: "+mDataSet.get(position).getUzywane());
     //   Log.d("onbindviewholder","Rozmiar: : "+mDataSet.size());
        if(mDataSet.get(position).getUzywane()==true) {
            if(mDataSet.get(position).getFolder()==false){
            for (int n = 0; n < mDataSet.size(); n++) {
                Log.d("onbindviewholder", "lista: " + n + " : " + mDataSet.get(n).getAdresAndroid());
            }
            //for (int n = 0; n < mDataSet.size(); n++) {

       //     Log.d("onbindviewholder", "listka getAndroid: : " + position + " : " + mDataSet.get(position).getAdresAndroid());
        //    Log.d("onbindviewholder", "listka getAdres: : " + position + " : " + mDataSet.get(position).getAdres());
            File plikk = new File(mDataSet.get(position).getAdresAndroid());
            Log.d("onbindviewholder", "if plik:" + plik.getName() + " == " + " plikk:" + plikk.getName());
            if (plik.getName().equals(plikk.getName()) ) {
                Log.d("onbindviewholder", "Zaszlo ");
                holder.mTextView2.setText("");
                //mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, false));
                toggleProductDescriptionHeight(holder.itemView);
                Bitmap bitmap = BitmapFactory.decodeFile(plikk.getAbsolutePath());
                holder.mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, true));

              //  holder.mCardView.findViewById(R.id.button6).setOnClickListener(new Klikacz(holder.mImageView,cont));
                przycisk2= (Button) holder.itemView.findViewById(R.id.button6);
                przycisk2.setOnClickListener(new Klikacz(holder.mImageView,cont));

            }

            //  }
        }else{
                File plikk = new File(mDataSet.get(position).getAdresAndroid());
                if (plik.getName().equals(plikk.getName())) {
                    Log.d("onbindviewholder", "Zaszlo ");
                    holder.mTextView2.setText("");
                    //mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, false));
                    toggleProductDescriptionHeight(holder.itemView);
                    Bitmap bitmap = BitmapFactory.decodeResource(cont.getResources(),R.drawable.ic_folder2);
                    holder.mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 220, 190, true));
                   // holder.mCardView.findViewById(R.id.button6).setOnClickListener(new Klikacz(holder.mImageView,cont));
                    przycisk2= (Button) holder.itemView.findViewById(R.id.button6);
                    przycisk2.setOnClickListener(new Klikacz(holder.mImageView,cont));
                }
            }


        }
        Log.d("klik","Bylem tu");

        przycisk= (Button) holder.itemView.findViewById(R.id.button5);
        przycisk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View itemView) {
                Log.d("klik","kliko");
                List<Linki> listka=PlikiLista.mDataSet;
                TextView nazwa= (TextView) holder.itemView.findViewById(R.id.tv_text);
                //Typeface face2= Typeface.createFromAsset(getAssets(),"fonts/Lato/Lato-Medium.ttf");
                String tekst= (String) nazwa.getText();
                Log.d("delete","lista rozmiar: "+listka.size());

                File pliko=null;
                boolean usun=false;
                for(int n=0;n<listka.size();n++){
                    pliko=null;
                    Log.d("delete","listka aderes android:: "+listka.get(n).getAdresAndroid());
                    if(listka.get(n).getAdresAndroid()!=null) {
                        pliko = new File(listka.get(n).getAdresAndroid());
                        Log.d("delete", "tekst:" + tekst + " plik:" + pliko.getName());


                        if (pliko.getName().equals(tekst)) {
                            BlankFragment.sfl.setRefreshing(true);
                            listka.get(n).setUzywane(false);
                            Log.d("delete", "Zaszlo");
                            pliko.delete();
                            //Cos tu nie gralo
                    /*    for(int m=0;m<mDataSet.size();m++){

                            File pliko2=new File(mDataSet.get(m).getAdres());
                            Log.d("mdataset","[] pliko:"+pliko.getName()+" == "+pliko2.getName());
                            if(pliko.getName().equals(pliko2.getName())){
                                Log.d("mdataset","Zaszlo");
                                mDataSet.get(m).setUzywane(false);
                            }
                        }*/
                            //BlankFragment.sfl.setRefreshing(true);
                            //   BlankFragment.sfl.onR
                            //  PlikiLista.kolejny.proces();
                            usun=true;


                        } else {
                            Log.d("delete", "NOPE");
                        }
                    }
                }
                if(usun==true) {
                    PlikiLista.listaByl=new ArrayList<BylJuzTaki>();
                    PlikiLista.mDataSet = listka;
                    Log.d("listka11", ": Teraz wypisuje mdataset przed seto:");
                    Log.d("listka11", ": Rozmiar mdataSet:"+PlikiLista.mDataSet.size());
                    for(int n=0;n<PlikiLista.mDataSet.size();n++){
                        if(PlikiLista.mDataSet.get(n).getAdres()!=null){
                            Log.d("listka11", ":" + n + ": mdataset getAdrese: "+PlikiLista.mDataSet.get(n).getAdres());
                        }
                        if(PlikiLista.mDataSet.get(n).getAdresAndroid()!=null){
                            Log.d("listka11", ":" + n + ": mdataset getAdresAndroid: "+PlikiLista.mDataSet.get(n).getAdresAndroid());
                        }
                        Log.d("listka11", ":" + n + ": mdataset getUzywane: "+PlikiLista.mDataSet.get(n).getUzywane());
                    }
                  //  PlikiLista.setMDataSeto();
                    PlikiLista.kolejny.proces();
                    PlikiLista.listaByl=new ArrayList<BylJuzTaki>();
                    PlikiLista.setMDataSeto();
                    Log.d("listka11", ": Teraz wypisuje mdataset po seto:");
                    Log.d("listka11", ": Rozmiar mdataSet:"+PlikiLista.mDataSet.size());
                    for(int n=0;n<PlikiLista.mDataSet.size();n++){
                        if(PlikiLista.mDataSet.get(n).getAdres()!=null){
                            Log.d("listka11", ":" + n + ": mdataset getAdrese: "+PlikiLista.mDataSet.get(n).getAdres());
                        }
                        if(PlikiLista.mDataSet.get(n).getAdresAndroid()!=null){
                            Log.d("listka11", ":" + n + ": mdataset getAdresAndroid: "+PlikiLista.mDataSet.get(n).getAdresAndroid());
                        }
                        Log.d("listka11", ":" + n + ": mdataset getUzywane: "+PlikiLista.mDataSet.get(n).getUzywane());
                    }
                    MyAdapter adapter = new MyAdapter(PlikiLista.mDataSet);
                    BlankFragment.rv.setAdapter(adapter);
                    BlankFragment.rv.invalidate();
                    BlankFragment.sfl.setRefreshing(false);
                }
                for(int n=0;n<listka.size();n++){
                    if(listka.get(n).getAdresAndroid()!=null) {
                        Log.d("listka", ":" + n + ": listka getAdresAndroid  :" + listka.get(n).getAdresAndroid());
                    }
                    if(listka.get(n).getAdres()!=null) {
                        Log.d("listka", ":" + n + ": listka getAdres  :" + listka.get(n).getAdres());
                    }
                    Log.d("listka", ":" + n + ": listka getUzuwane: "+listka.get(n).getUzywane());
                }
                Log.d("listka", ": Teraz wypisuje mdataset:");
                Log.d("listka", ": Rozmiar mdataSet:"+PlikiLista.mDataSet.size());
                for(int n=0;n<PlikiLista.mDataSet.size();n++){
                    if(PlikiLista.mDataSet.get(n).getAdres()!=null){
                        Log.d("listka", ":" + n + ": mdataset getAdrese: "+PlikiLista.mDataSet.get(n).getAdres());
                    }
                    if(PlikiLista.mDataSet.get(n).getAdresAndroid()!=null){
                        Log.d("listka", ":" + n + ": mdataset getAdresAndroid: "+PlikiLista.mDataSet.get(n).getAdresAndroid());
                    }
                    Log.d("listka", ":" + n + ": mdataset getUzywane: "+PlikiLista.mDataSet.get(n).getUzywane());
                }
             //   mDataSet=PlikiLista.lista;
            }

        });
    }

    @Override
    public int getItemCount() {
       // Log.d("rozm","Rozmiar: "+mDataSet.size());
        return mDataSet.size();
    }
    public static void setmDataSet(List<Linki> mDataSet1){
        mDataSet=mDataSet1;
    }
    public List<Linki> getmDataSet(){
        return mDataSet;
    }
}
