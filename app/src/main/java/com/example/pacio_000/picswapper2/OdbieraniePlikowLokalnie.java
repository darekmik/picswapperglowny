package com.example.pacio_000.picswapper2;

import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patryk on 20.09.2016.
 */
public class OdbieraniePlikowLokalnie extends AsyncTask<Void,Void,Boolean> {
    private File pliko;
    private Uzytkownik uz;
    public OdbieraniePlikowLokalnie(File pliko, Uzytkownik uz){
        this.pliko=pliko;
        this.uz=uz;
    }
    @Override
    protected Boolean doInBackground(Void... params) {
        PlikiLista.foldery2=new ArrayList<Linki>();
        PlikiLista.pliczki=pliko.listFiles();
        Log.d("PlikGlowny",":"+pliko);
        File[] test=PlikiLista.pliczki;
        List<Linki> lista=new ArrayList<Linki>();
        Log.d("plikiZdysku", "Przed sprawdzaniem czy zawiera png");
        for(int n=0;n<test.length;n++){
            //boolean spro=false;
            Log.d("plikiZdysku", ": "+n+" : "+test[n].getName());
            String tmp=test[n].getName();
            char[] tabka=new char[tmp.length()];
            Log.d("charat",": wielkosc testu: "+test[n].getName().length());
            for(int m=0;m<test[n].getName().length();m++){
                tabka[m]=tmp.charAt(m);
            }
           /* if(test[n].getName().contains(".jpg") || test[n].getName().contains("IMG") ){
                Log.d("Zasadsodkasod",":"+test[n].getName());

                test[n].delete();
            }else{
                Log.d("Zasadsodkasod",":NIE ZACHODZI: "+test[n].getName());
            }*/
            if (test[n].getName().contains(".png")) {
                Log.d("plikiZdysku", "zawiera");
                Log.d("plikiZdysku", "plik: " + test[n].getAbsolutePath());

                lista.add(new Linki(test[n].getAbsolutePath(),uz,true,false));

                //  test[n].delete();
                // Log.d("testPliki","lista: "+lista.get(n).getAdresAndroid());
            }else  if (test[n].isDirectory() && !test[n].getName().equals("instant-run")){
                Log.d("plikiZdysku",": folder: "+test[n].getAbsolutePath());
                //usuwanie(test[n]);
                lista.add(new Linki(test[n].getAbsolutePath(),uz,true,true));
                if(PlikiLista.foldery==null){
                    PlikiLista.foldery=new ArrayList<Linki>();
                   // PlikiLista.foldery.add(new Linki());
                }
                PlikiLista.foldery.add(new Linki(test[n].getAbsolutePath(),uz,true,true));
            }
        }
        for(int n=0;n<PlikiLista.foldery.size();n++){
            File plik=new File(PlikiLista.foldery.get(n).getAdresAndroid());

            Log.d("Rekurencja","Plik na starcie: "+n+" : "+plik.getAbsolutePath());
            PlikiLista.foldery2.add(new Linki(plik.getAbsolutePath(),uz,true,true));
            folderoRekurencja(plik);
        }

        Log.d("plikiZdysku", "Sprawdzanie listy przed wyslaniem do globalne");
        for(int n=0;n<lista.size();n++){
            Log.d("plikiZdysku", "lista: spr: "+n+lista.get(n).getAdresAndroid());
        }
        //Linki.lista=lista;
        if(PlikiLista.mDataSet==null){
            PlikiLista.mDataSet=new ArrayList<Linki>();
        }
        PlikiLista.mDataSet=lista;
        // PlikiLista.setMDataSeto();

        Kolejny.lista22=PlikiLista.mDataSet;

        Log.d("plikiZdysku","Rozmiar lista2: "+Kolejny.lista22.size());
        for(int n=0;n<Kolejny.lista22.size();n++){
            Log.d("plikiZdysku","lista: "+Kolejny.lista22.get(n).getAdresAndroid());
        }
        return true;
    }
    public void folderoRekurencja(File plik){
       File[] pliki=plik.listFiles();
        Log.d("Rekurencja","Plik nad ktorym pracuje: "+plik.getAbsolutePath());
        Log.d("Rekurencja","Wielkosc listy z plikami:"+pliki.length);
        for(int n=0;n<pliki.length;n++){
            if(pliki[n].isDirectory()){
                Log.d("Rekurencja","Dodaje na: "+n+" : "+pliki[n].getAbsolutePath());
              PlikiLista.foldery2.add(new Linki(pliki[n].getAbsolutePath(),uz,true,true));
                folderoRekurencja(pliki[n]);
            }
        }
    }
}
