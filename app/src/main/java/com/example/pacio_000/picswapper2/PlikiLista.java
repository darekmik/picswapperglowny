package com.example.pacio_000.picswapper2;

import android.text.util.Linkify;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patryk on 09.08.2016.
 */
public  class PlikiLista {
    public static int tmpLiczba=0;
    public static String sciezka;
    public static boolean pierwszy;
    public static List<Linki> lista2=new ArrayList<Linki>();
    public static Uzytkownik uz;
    public static File[] pliczki;
    public static List<Linki> lista;
    public static Kolejny kolejny;
    public static List<Linki> mDataSet;
    public static List<List<Linki>> tmpList;
    public static List<Linki> foldery;
    public static List<Linki> foldery2;
    public static boolean alternatywny=false;
    public static List<BylJuzTaki> listaByl=new ArrayList<BylJuzTaki>();
    public static String adres;
    public static BylJuzTaki getListaByl(int n){
        return listaByl.get(n);
    }
    public static void addListaByl(BylJuzTaki n){
        listaByl.add(n);
    }
    public static void undoView(){
        Log.d("mdatasetDlaundo","przed Tmp liczba: "+tmpLiczba);

        tmpLiczba--;


        Log.d("mdatasetDlaundo","Tmp liczba: "+tmpLiczba);
        if(tmpLiczba<0){
            mDataSet=new ArrayList<Linki>();
            List<Linki> tmpo;
            tmpo=tmpList.get(0);
            for(int n=0;n<tmpo.size();n++){
                mDataSet.add(tmpo.get(n));
            }
            alternatywny=false;
        }else {
            mDataSet=new ArrayList<Linki>();
            List<Linki> tmpo=new ArrayList<Linki>();
            tmpo=tmpList.get(tmpLiczba);
            Log.d("tmp4647","Wypisuje tmpo: ");
            for(int n=0;n<tmpo.size();n++){
                Log.d("tmp4647",": "+n+" : "+tmpo.get(n).getAdres());
                mDataSet.add(tmpo.get(n));
            }
          //  mDataSet = tmpList.get(tmpLiczba);
        }
    /*    Log.d("undoS","Wypisuje wszystkie mdatasety: ");
        for(int n=0;n<tmpList.size();n++){
            Log.d("undoS", "======================================================================== ");
            Log.d("undoS", ": Lista numer: "+n );
            List<Linki> tmpLinki=tmpList.get(n);
            for(int m=0;m<tmpLinki.size();m++) {
                Log.d("undoS", ":" + m + " : adres : " + mDataSet.get(m).getAdres());
                Log.d("undoS", ":" + m + " : adresAndroid : " + mDataSet.get(m).getAdresAndroid());
                Log.d("undoS", ":" + m + " : Uzywanie : " + mDataSet.get(m).getUzywane());
            }
            Log.d("undoS", "======================================================================== ");
        }*/
        Log.d("undoS", "tmpLiczba: "+tmpLiczba);
        MyAdapter adapter1 = new MyAdapter(PlikiLista.mDataSet);
        PlikiLista.listaByl=new ArrayList<BylJuzTaki>();
        PlikiLista.setMDataSeto();
        Log.d("mdataSetPliki","Wypisuje po seto");
        for(int n=0;n<PlikiLista.mDataSet.size();n++){
            Log.d("mdataSetPliki",": "+n+" : "+PlikiLista.mDataSet.get(n).getAdres());
        }
        BlankFragment.rv.setAdapter(adapter1);
        BlankFragment.rv.invalidate();


    }
    public static void tmpView(String adres){
        if(tmpList==null){
            Log.d("tmpView","NULLL O ZASZLO: ");
            tmpList=new ArrayList<List<Linki>>();
        }
        alternatywny=true;
        tmpLiczba++;
        List<Linki> tmpko=new ArrayList<Linki>();
        for(int n=0;n<mDataSet.size();n++){
            tmpko.add(mDataSet.get(n));
        }
        tmpList.add(tmpko);
      /*  Log.d("undoS12","Wypisuje wszystkie w momecie dodawania mdatasety: ");
        for(int n=0;n<tmpList.size();n++){
            Log.d("undoS12", "======================================================================== ");
            Log.d("undoS12", ": Lista numer: "+n );
            List<Linki> tmpLinki=tmpList.get(n);
            for(int m=0;m<tmpLinki.size();m++) {
                Log.d("undoS12", ":" + m + " : adres : " + mDataSet.get(m).getAdres());
                Log.d("undoS12", ":" + m + " : adresAndroid : " + mDataSet.get(m).getAdresAndroid());
                Log.d("undoS12", ":" + m + " : Uzywanie : " + mDataSet.get(m).getUzywane());
            }
            Log.d("undoS", "======================================================================== ");
        }*/
        Log.d("tmpView","Wypisuje mdataset w tmpView tmp Liczna: "+tmpLiczba);
        for(int n=0;n<mDataSet.size();n++){
            Log.d("tmpView",":" +n+" : adres : "+mDataSet.get(n).getAdres());
            Log.d("tmpView",":" +n+" : adresAndroid : "+mDataSet.get(n).getAdresAndroid());
            Log.d("tmpView",":" +n+" : Uzywanie : "+mDataSet.get(n).getUzywane());
        }
       // File pliczo=new File("/data/user/0/com.example.pacio_000.picswapper2/files/wielblady/nowy");
       // pliczo.delete();
        Log.d("adreso123",": adres: "+adres);
        File dir=new File(adres);
        if(dir.isDirectory()){
            Log.d("adreso123","TO FOLDER");
        }else{
            Log.d("adreso123","TO PLIK");
        }
        File[] plik=dir.listFiles();
        mDataSet.clear();
        Log.d("adreso123456","Wielkosc plik: "+plik.length);
        for(int n=0;n<plik.length;n++){
            Log.d("adreso123456",":  "+n+": "+plik[n]);
        }
        for(int n=0;n<plik.length;n++){
            if(plik[n].isDirectory()) {
                Log.d("pliczkicheck",": folder:  "+ plik[n].getName());
                File pliko=PlikiLista.giveMeMyAdrres(plik[n].getAbsolutePath());
                mDataSet.add(new Linki(plik[n].getAbsolutePath(),pliko.getAbsolutePath(), uz, true,true));
                Log.d("mdatasetcheck",": mdatasetAdres:  "+ mDataSet.get(n).getAdres());
                Log.d("mdatasetcheck",": mdatasetFolder:  "+ mDataSet.get(n).getFolder());
            }else{

                Log.d("pliczkicheck",": pliko:  "+ plik[n].getName());
                File pliko=PlikiLista.giveMeMyAdrres(plik[n].getAbsolutePath());
             //   Log.d("testTu123",": pliko: "+pliko.getAbsolutePath());
                mDataSet.add(new Linki(plik[n].getAbsolutePath(),pliko.getAbsolutePath(),uz,true,false));
            }
        }
        Log.d("mdataSetPliki","Wypisuje przed seto");
        for(int n=0;n<PlikiLista.mDataSet.size();n++){
            Log.d("mdataSetPliki",": "+n+" : "+PlikiLista.mDataSet.get(n).getAdres());
            Log.d("mdataSetPliki"," folder: : "+PlikiLista.mDataSet.get(n).getFolder());
        }
        MyAdapter adapter1 = new MyAdapter(PlikiLista.mDataSet);
        PlikiLista.listaByl=new ArrayList<BylJuzTaki>();
        PlikiLista.setMDataSeto();
        Log.d("mdataSetPliki","Wypisuje po seto");
        for(int n=0;n<PlikiLista.mDataSet.size();n++){
            Log.d("mdataSetPliki",": "+n+" : "+PlikiLista.mDataSet.get(n).getAdres());
        }
        BlankFragment.rv.setAdapter(adapter1);
        BlankFragment.rv.invalidate();
    }
    public static void setMDataSeto(){
        List<Linki> lol=CollectionSmieciarz.work(mDataSet);
        Log.d("listka","Wypisuje liste lol w Pliki Lista:");
        for(int n=0;n<lol.size();n++){
            if(lol.get(n).getAdresAndroid()!=null) {
                Log.d("listka", ":" + n + ": lol getAdresAndroid  :" + lol.get(n).getAdresAndroid());
            }
            if(lol.get(n).getAdres()!=null) {
                Log.d("listka", ":" + n + ": lol getAdres  :" + lol.get(n).getAdres());
            }
            Log.d("listka", ":" + n + ": lol getUzuwane: "+lol.get(n).getUzywane());
        }
        MyAdapter.setmDataSet(lol);
        mDataSet=MyAdapter.getMDataSet();
        Log.d("listka", ":Wypisywac teraz bede bezposrednio mdataSet:");
        for(int n=0;n<mDataSet.size();n++){
            if(mDataSet.get(n).getAdresAndroid()!=null) {
                Log.d("listka", ":" + n + ": lol getAdresAndroid  :" + mDataSet.get(n).getAdresAndroid());
            }
            if(mDataSet.get(n).getAdres()!=null) {
                Log.d("listka", ":" + n + ": lol getAdres  :" + mDataSet.get(n).getAdres());
            }
            Log.d("listka", ":" + n + ": lol getUzuwane: "+mDataSet.get(n).getUzywane());
        }
    }
    public static int ListaBylRozmiar(){
        return listaByl.size();
    }
    public static File giveMeMyAdrres(String plik){
        File pliko12=new File(plik);
       /* if(pliko12.getName().contains(".jpg") || pliko12.getName().contains("IMG") ){
            Log.d("Zasadsodkasod",":"+pliko12.getName());

            pliko12.delete();
        }else{
            Log.d("Zasadsodkasod",":NIE ZACHODZI: "+pliko12.getName());
        }*/
        Log.d("giveMeMyAdrres11121",": Wypisuje liste2:  ");
        for(int n=0;n<lista2.size();n++){
            Log.d("giveMeMyAdrres11121",": "+n+" :"+lista2.get(n).getAdres());
            Log.d("giveMeMyAdrres11121",": FOLDER: :"+lista2.get(n).getFolder());
        }
      /*  for(int n=0;n<lista2.size();n++){
            mDataSet.add(lista2.get(n));
        }
        */
        Log.d("giveMeMyAdrres111",": adres wejsciowy:  "+plik);
        File zwrot=null;
        Log.d("giveMeMyAdrres111",": Lista rozmiar "+ PlikiLista.lista2.size());
        Log.d("giveMeMyAdrres111",": PlikLista pierwszy "+ PlikiLista.pierwszy);
        //int n=0;
        for(int n=0;n<lista2.size();n++){
            File pliko=new File(lista2.get(n).getAdres());
            File pliko2=new File(plik);
            Log.d("giveMeMyAdrres111",": pliko:  "+ pliko.getName()+" plik: "+pliko2.getName());
            if(pliko.getName().equals(pliko2.getName())){
                zwrot=new File(pliko.getAbsolutePath());
            }
        }
        Log.d("giveMeMyAdrres111",": adres wyjsciowy:  "+zwrot.getAbsolutePath());
        return zwrot;
    }
}
