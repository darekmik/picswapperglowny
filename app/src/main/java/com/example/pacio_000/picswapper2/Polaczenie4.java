package com.example.pacio_000.picswapper2;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patryk on 14.09.2016.
 */
public class Polaczenie4 extends AsyncTask<Void,Void,Uzytkownik> {
        private Uzytkownik uz;
        private boolean dzialanie;
        private  List<TypyFile> lista;
    public Polaczenie4(Uzytkownik uz){
        this.uz=uz;
        dzialanie=false;
    }

    @Override
    protected Uzytkownik doInBackground(Void... params) {
        Socket sock = null;
        InputStream in = null;
        OutputStream out = null;
        ObjectOutputStream oos=null;
        ObjectInputStream ois = null;
        try {
            try {
                Log.d("wPol4","Jestem w polaczeniu4");
                sock=new Socket("13.74.155.28",8888);
                Log.d("wPol4","Polaczono");
                in=sock.getInputStream();
                out=sock.getOutputStream();
                oos=new ObjectOutputStream(out);
                ois=new ObjectInputStream(in);

                out.write(5);
                out.flush();
                int zap=in.read();
                out.write(uz.getID());
                out.flush();
                lista= (List<TypyFile>) ois.readObject();
                for(int n=0;n<lista.size();n++){
                    Log.d("dupercia",":"+lista.get(n).getNazwa());
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

                ois.close();
                oos.close();
                out.close();
                in.close();
                sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        dzialanie=true;
        return null;
    }
    public boolean getDzialanie(){
        return dzialanie;
    }
    public List<TypyFile> getLista(){

        return lista;
    }
}
