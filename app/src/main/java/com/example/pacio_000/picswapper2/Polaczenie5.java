package com.example.pacio_000.picswapper2;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by Patryk on 23.09.2016.
 */
public class Polaczenie5 extends AsyncTask<Void,Void,Boolean> {
    private Uzytkownik uz;
    private Socket sock=null;
    private InputStream in=null;
    private OutputStream out=null;
    private ObjectOutputStream oos=null;
    private ObjectInputStream ois=null;
    DataOutputStream dos=null;
    String adres;
    File plik;
    public Polaczenie5(Uzytkownik uz, String adres,File plik){
        this.uz=uz;
        this.adres=adres;
        this.plik=plik;
    }
    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            sock=new Socket("13.74.155.28",8888);
            in=sock.getInputStream();
            out=sock.getOutputStream();
            oos=new ObjectOutputStream(out);
            ois=new ObjectInputStream(in);
            dos=new DataOutputStream(out);
            out.write(6);
            out.flush();
            int zap=in.read();
            out.write(uz.getID());
            out.flush();
            dos.writeChars(adres);
            byte[] tablica=new byte[(int)plik.length()];
            FileInputStream fis=new FileInputStream(plik);
            BufferedInputStream bis=new BufferedInputStream(fis);
            bis.read(tablica,0,tablica.length);
            PlikInfo pi=new PlikInfo(plik.getName(),1,1,"paprapra",1,tablica);
            bis.close();
            fis.close();
            oos.writeObject(pi);

            dos.close();
            ois.close();
            oos.close();
            out.close();
            in.close();
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
